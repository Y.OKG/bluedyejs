/**
 * BlueDyeJS v1.1
 * by Yazid Slila @Y.OKG
 * MIT License
 */

(function(r, e) { typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = e() : typeof define === 'function' && define.amd ? define(e) : (r.bluedye = e()); }(this, (function() {
  'use strict';
  let _C = function(n, b) {
      var k = n.toString(b),
        k = k.length == 1 ? '0' + k : k;
      return k
    },
    _E = Math.floor,
    _N = Math.min,
    _P = Math.pow,
    _X = Math.max,
    _T = function(a) { return _X(_N(_E(a), 255), 0) },
    _d = function(a, g) { return _T(a + (255 - a) / 8 * g) },
    _r = function(a, g) { return _T(a - a / 8 * g) },
    _t = function(n) { return _F(n / 17) * 17 },
    _B = function(a, b) { return _T((a * (100 - b) + (255 - a) * b) / 100) },
    _l = function(x) { var e = _F(x * (Math.log(x) / Math.log(16)) / 2) || 0; return e },
    _h = function(x) { var e = _F(x * (Math.log(x / 10) / Math.log(6)) / 2) || 0; return e },
    _H = function(A) {
      A = typeof A == 'string' ? A : A + '',
        A = A.toLocaleLowerCase();
      return parseInt(A, 16)
    },
    _Z = function(a) {
      if (a.length == 7) {
        var r = _H(a.substr(1, 2)),
          g = _H(a.substr(3, 2)),
          b = _H(a.substr(5, 2))
      }
      if (a.length == 4) {
        var r = _H(a.substr(1, 1) + a.substr(1, 1)),
          g = _H(a.substr(2, 1) + a.substr(2, 1)),
          b = _H(a.substr(3, 1) + a.substr(3, 1))
      }
      return "rgb(" + r + ',' + g + ',' + b + ')'
    },
    _fe = function(a, f) {
      var b = [];
      for (let l = 0; l < a.length; l++) {
        b[l] = f(a[l]);
      }
      return b;
    },
    rgb = function(r, g, b) {
      return [r, g, b]
    },
    rgba = function(r, g, b, a) {
      return [r, g, b, a]
    },
    _v = function(a) {
      var e = [],
        r = [];
      for (var i = 0; i < a.length; i++) {
        r[i] = _E(Number(a[i]) * 255);
        e[i] = _E(Number(a[i]))
      }
      e = e[0] <= 1 && e[1] <= 1 && e[2] <= 1 ? r : e;
      return e
    };
  var bluedye = function(a) { return new bluedye.y.color(a) }
  bluedye.y = bluedye.prototype = {
    color: function(a) {
      var s = [0, 0, 0];
      var a = a.replace(/\s/g, '');
      if (typeof a == 'string') {
        if (a.replace(/#[0-9a-f]+/i, '') == '') {
          var a = _Z(a);
        }
        if (a.replace(/rgba*\([\d,\.]+\)/i, '') == '') {
          var s = eval(a);
        }
      }
      var s = _fe(s, function(x) { return _T(x) })
      this.red = s[0] || 0;
      this.green = s[1] || 0;
      this.blue = s[2] || 0;
      this.alpha = s[3] ?? 1;
      return this
    },
    xred: function(a) {
      this.red = _T(a);
      return this
    },
    xgreen: function(a) {
      this.green = _T(a);
      return this
    },
    xblue: function(a) {
      this.blue = _T(a);
      return this
    },
    xalpha: function(a) {
      this.alpha = _X(_N(a, 1), 0);
      return this
    },
    negative: function() {
      this.red = 255 - this.red;
      this.green = 255 - this.green;
      this.blue = 255 - this.blue;
      return this
    },
    gray: function() {
      var t = (this.red + this.green + this.blue) / 3;
      this.red = t;
      this.green = t;
      this.blue = t;
      return this
    },
    grey: function() {
      return this.gray()
    },
    max: function(a) {
      var a = _N(a, 255);
      this.red = _X(this.red, a);
      this.green = _X(this.green, a);
      this.blue = _X(this.blue, a);
      return this
    },
    min: function(a) {
      var a = _X(a, 0);
      this.red = _N(this.red, a);
      this.green = _N(this.green, a);
      this.blue = _N(this.blue, a);
      return this
    },
    dark: function(a) {
      var a = _N(_X(a, 0), 8);
      this.red = _r(this.red, a);
      this.green = _r(this.green, a);
      this.blue = _r(this.blue, a);
      return this
    },
    light: function(a) {
      var a = _N(_X(a, 0), 8);
      this.red = _d(this.red, a);
      this.green = _d(this.green, a);
      this.blue = _d(this.blue, a);
      return this
    },
    greenToRed: function() {
      var a = this.red;
      this.red = this.green;
      this.green = this.blue;
      this.blue = a;
      return this
    },
    redToGreen: function() {
      var a = this.blue;
      this.blue = this.green;
      this.green = this.red;
      this.red = a;
      return this
    },
    add: function(a) {
      this.red = _T(this.red + a);
      this.green = _T(this.green + a);
      this.blue = _T(this.blue + a);
      return this
    },
    mix: function(a, b, c) {
      var a = bluedye(a),
        x = a.red,
        y = a.green,
        z = a.blue,
        u = b + c,
        b = b / u;
      this.red = _T(this.red * b + x * (1 - b));
      this.green = _T(this.green * b + y * (1 - b));
      this.blue = _T(this.blue * b + z * (1 - b));
      return this
    },
    turn: function(a) {
      this.red = (this.red + a) % 256;
      this.green = (this.green + a) % 256;
      this.blue = (this.blue + a) % 256;
      return this
    },
    expo: function() {
      if (this.alpha == 1) return `rgb(${this.red},${this.green},${this.blue})`;
      return `rgba(${this.red},${this.green},${this.blue},${this.alpha})`;
    },
    hex: function() {
      return '#' + _fe([this.red, this.green, this.blue], function(x) { return _C(x, 16).toLocaleUpperCase() }).join('')
    },
  }
  bluedye.y.color.prototype = bluedye.y;
  bluedye.add = function(a) {
    a = typeof a === 'object' ? a : {};
    for (let k in a) {
      bluedye[k] = bluedye[k] || a[k];
    }
    return bluedye;
  };
  bluedye.add({
    build: 2,
    version: [1, 1],
  })
  window.bluedye = window.$bd = bluedye
})));
